# Fetish Reference
Assertiveness - Assert operations  
[C Reference](c_reference/assertiveness.md)  
[Fetlang Reference](reference/assertiveness.md)  

Core - The included-by-default fetish that defines the language  
[C Reference](c_reference/core.md)  
[Fetlang Reference](reference/core.md)  

Obedience - File interaction  
[C Reference](c_reference/obedience.md)  
[Fetlang Reference](reference/obedience.md)  

